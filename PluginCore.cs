﻿using System;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Timers;
using System.Linq;
using System.Xml;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using MyClasses.MetaViewWrappers;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using VirindiViewService.Controls;
using System.Data;
using System.Data.Linq;

namespace QuestTracker
{
    //Attaches events from core
    [WireUpBaseEvents]

    //View (UI) handling
    [MVView("QuestTracker.mainView.xml")]
    [MVWireUpControlEvents]
    //
    // FriendlyName is the name that will show up in the plugins list of the decal agent (the one in windows, not in-game)
    // View is the path to the xml file that contains info on how to draw our in-game plugin. The xml contains the name and icon our plugin shows in-game.
    // The view here is SamplePlugin.mainView.xml because our projects default namespace is SamplePlugin, and the file name is mainView.xml.
    // The other key here is that mainView.xml must be included as an embeded resource. If its not, your plugin will not show up in-game.

    [FriendlyName("QuestTracker")]
    public class PluginCore : PluginBase
    {
        private bool shouldEatQuests = false;
        private bool buttonClicked = false;

        //readonly HudStaticText headerText;
        /// <summary>
        /// This is called when the plugin is started up. This happens only once.
        /// </summary>
        protected override void Startup()
        {
            try
            {
                // This initializes our static Globals class with references to the key objects your plugin will use, Host and Core.
                // The OOP way would be to pass Host and Core to your objects, but this is easier.
                Globals.Init("QuestTracker", Host, Core);

                //Initialize the view.
                MVWireupHelper.WireupStart(this, Host);
                CoreManager.Current.ChatBoxMessage += new EventHandler<ChatTextInterceptEventArgs>(Current_ChatBoxMessage);
                //Util.LoadQuestLookupXML();
                //Util.UpdateFileFromServer();
            }
            catch (Exception ex) { Util.LogError(ex); }
        }

        /// <summary>
        /// This is called when the plugin is shut down. This happens only once.
        /// </summary>
        protected override void Shutdown()
        {
            try
            {
                //Destroy the view.
                MVWireupHelper.WireupEnd(this);
                CoreManager.Current.ChatBoxMessage -= new EventHandler<ChatTextInterceptEventArgs>(Current_ChatBoxMessage);
            }
            catch (Exception ex) { Util.LogError(ex); }
        }


        public string GetFriendlyTimeDifference(TimeSpan difference)
        {
            string output = "";

            if (difference.TotalDays > 0) output += difference.Days.ToString() + "d ";
            if (difference.TotalHours > 0) output += difference.Hours.ToString() + "h ";
            if (difference.TotalMinutes > 0) output += difference.Minutes.ToString() + "m ";
            if (difference.TotalSeconds > 0) output += difference.Seconds.ToString() + "s ";

            return output;
        }

        public string GetFriendlyTimeDifference(long difference)
        {
            return GetFriendlyTimeDifference(TimeSpan.FromSeconds(difference));
        }




        public void CreateDataTable()
        {
            questDataTable.Columns.Add("questKeyTest");
            questDataTable.Columns.Add("solveCountTest");
            //questDataTable.Columns.Add("completedOnTest");
            questDataTable.Columns.Add("questDescriptionTest");
            questDataTable.Columns.Add("maxCompletionsTest");
            questDataTable.Columns.Add("repeatTimeTest");
            questDataTable.Columns.Add("questType");
        }

        //DataTable questDataTable = new DataTable();
        private Dictionary<string, string> questList = new Dictionary<string, string>();
        private Dictionary<string, string> killTaskList = new Dictionary<string, string>();
        private static readonly Regex myQuestRegex = new Regex(@"(?<questKey>\S+) \- (?<solveCount>\d+) solves \((?<completedOn>\d{0,11})\)""?((?<questDescription>.*)"" (?<maxCompletions>.*) (?<repeatTime>\d{0,6}))?.*$");
        //private static readonly Regex myQuestRegex = new Regex(@"(?<questKey>\S+) \- (?<solveCount>\d+) solves? \((?<completedOn>\d{0,11})\)((?<questDescription>.*) -?\d+ (?<repeatTime>\d+))?.*$");
        private static readonly Regex killTaskRegex = new Regex(@"killtask|killcount|slayerquest|totalgolem.*dead");
        DataTable questDataTable = new DataTable();

        public void Current_ChatBoxMessage(object sender, ChatTextInterceptEventArgs e)
        {
            try
            {
                Match match = myQuestRegex.Match(e.Text);

                if (match.Success)
                {

                    if (questDataTable.Rows.Count == 0)
                    {
                        //Util.WriteToChat("does not exit");
                        CreateDataTable();
                    }
                    else
                    {
                        //Util.WriteToChat("exists");
                    }

                    string questKey = match.Groups["questKey"].Value;
                    string solveCount = match.Groups["solveCount"].Value;
                    //string completedOn = match.Groups["completedOn"].Value;
                    string questDescription = match.Groups["questDescription"].Value;
                    string maxCompletions = match.Groups["maxCompletions"].Value;
                    //string repeatTime = match.Groups["repeatTime"].Value;
                    long availableOnEpoch = 0;
                    string questTimerstr = "";
                    string questType = "";
                    long todayEpoch = (long)((DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds);
                    bool dupe = false;
                    if (Int32.TryParse(match.Groups["completedOn"].Value, out int completedOn))
                    {
                        availableOnEpoch = completedOn;
                    }
                    if (Int32.TryParse(match.Groups["repeatTime"].Value, out int repeatTime))
                    {
                        availableOnEpoch += repeatTime;
                    }

                    if (todayEpoch > availableOnEpoch)
                    {
                        questTimerstr = "Ready";
                    }
                    else
                    {
                        questTimerstr = GetFriendlyTimeDifference(availableOnEpoch - todayEpoch);
                    }


                    Match matchKillTask = killTaskRegex.Match(questKey);

                    if (matchKillTask.Success)
                    {
                        //Util.WriteToChat("test");
                        questType = "killTask";
                    }
                    if (maxCompletions == "1")
                    {
                        questType = "oneTimeQuest";
                    }


                    DataRow newDTRow = questDataTable.NewRow();
                    foreach (DataRow row in questDataTable.Rows)
                    {
                        if (row["questKeyTest"].ToString() == questKey)
                        {
                            // Util.WriteToChat(questKey + " already exists");
                            dupe = true;
                            // Util.WriteToChat(questKey + ": duplicate");
                            row["solveCountTest"] = solveCount;
                            //newDTRow["completedOnTest"] = completedOn;
                            if(questDescription == "")
                            {
                                row["questDescriptionTest"] = questKey;
                            }
                            else
                            {
                                row["questDescriptionTest"] = questDescription;
                            }
                            
                            row["maxCompletionsTest"] = maxCompletions;
                            row["repeatTimeTest"] = questTimerstr;
                            row["questType"] = questType;
                        }
                    }


                    if (dupe == true)
                    {
                        //Util.WriteToChat(questKey + ": duplicate");
                        //newDTRow["solveCountTest"] = solveCount;
                        ////newDTRow["completedOnTest"] = completedOn;
                        //newDTRow["questDescriptionTest"] = questDescription;
                        //newDTRow["maxCompletionsTest"] = maxCompletions;
                        //newDTRow["repeatTimeTest"] = questTimerstr;
                        //newDTRow["questType"] = questType;
                    }
                    else
                    {

                        //if(row["questKeyTest"])
                        newDTRow["questKeyTest"] = questKey;
                        newDTRow["solveCountTest"] = solveCount;
                        //newDTRow["completedOnTest"] = completedOn;
                        if (questDescription == "")
                        {
                            newDTRow["questDescriptionTest"] = questKey;
                        }
                        else
                        {
                            newDTRow["questDescriptionTest"] = questDescription;
                        }
                        newDTRow["maxCompletionsTest"] = maxCompletions;
                        newDTRow["repeatTimeTest"] = questTimerstr;
                        newDTRow["questType"] = questType;
                        questDataTable.Rows.Add(newDTRow);
                    }

                    if (shouldEatQuests) {
                        e.Eat = true;
                    }
                }
            }
            catch (Exception ex) { Util.LogError(ex); }
        }


        public void PopulateQuests()
        {
            try
            {
                buttonClicked = true;
                shouldEatQuests = true;

                    myQuestList.Clear();
                    myKillTaskList.Clear();
                    myOneTimeList.Clear();

                    CoreManager.Current.Actions.InvokeChatParser("/myquests");

                    IListRow killTaskRow = myKillTaskList.Add();
                    killTaskRow[0][0] = "QuestName";
                    killTaskRow[1][0] = "solveCount";
                    killTaskRow[2][0] = "maxCompletions";

                    IListRow oneTimeRow = myOneTimeList.Add();
                    oneTimeRow[0][0] = "QuestName";

                    IListRow myQuestRow = myQuestList.Add();
                    myQuestRow[0][0] = "QuestName";
                    myQuestRow[1][0] = "repeatTime";
                    myQuestRow[2][0] = "solveCount";


                    System.Threading.Timer timer = null;
                    timer = new System.Threading.Timer((obj) =>
                    {
                        try
                        {
                            shouldEatQuests = false;

                            if (questDataTable.Rows.Count >= 1)
                            {
                                DataView dv = questDataTable.DefaultView;
                                dv.Sort = "repeatTimeTest ASC";
                                DataTable sortedQuestDataTable = dv.ToTable();


                                foreach (DataRow row in sortedQuestDataTable.Rows)
                                {

                                    string QuestName = row["questDescriptionTest"].ToString();

                                    if (row["questType"].ToString() == "killTask")
                                    {
                                        killTaskRow = myKillTaskList.Add();
                                        killTaskRow[0][0] = QuestName;
                                        killTaskRow[1][0] = row["solveCountTest"].ToString();
                                        killTaskRow[2][0] = row["maxCompletionsTest"].ToString();
                                    }
                                    else if (row["questType"].ToString() == "oneTimeQuest")
                                    {
                                        oneTimeRow = myOneTimeList.Add();
                                        oneTimeRow[0][0] = QuestName;
                                    }
                                    else
                                    {
                                        myQuestRow = myQuestList.Add();
                                        myQuestRow[0][0] = QuestName;
                                        myQuestRow[1][0] = row["repeatTimeTest"].ToString();
                                        myQuestRow[2][0] = row["solveCountTest"].ToString();
                                    }
                                }
                            }
                            buttonClicked = false;
                            timer.Dispose();
                        }
                        catch (Exception ex) { Util.LogError(ex); }

                    },
                                null, 500, System.Threading.Timeout.Infinite);
                }
            catch (Exception ex) { Util.LogError(ex); }
        }


        [MVControlReference("myQuestList")]
        private IList myQuestList = null;
        [MVControlReference("myKillTaskList")]
        private IList myKillTaskList = null;
        [MVControlReference("myOneTimeList")]
        private IList myOneTimeList = null;

        [MVControlEvent("PopulateQuestList", "Click")]
        void PopulateQuestList_Click(object sender, MVControlEventArgs e)
        {
            try
            {
                if (buttonClicked == false)
                {
                    PopulateQuests();
                }
                
            }
            catch (Exception ex) { Util.LogError(ex); }
        }

        [MVControlEvent("PopulateKillTaskList", "Click")]
        void PopulateKillTaskList_Click(object sender, MVControlEventArgs e)
        {
            try
            {
                if (buttonClicked == false)
                {
                    PopulateQuests();
                }
            }
            catch (Exception ex) { Util.LogError(ex); }
        }

        [MVControlEvent("PopulateOneTimeList", "Click")]
        void PopulateOneTimeList_Click(object sender, MVControlEventArgs e)
        {
            try
            {
                if (buttonClicked == false)
                {
                    PopulateQuests();
                }
            }
            catch (Exception ex) { Util.LogError(ex); }
        }
    }
}