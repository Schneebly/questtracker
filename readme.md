# About
A quest tracking plugin.

# Installation:
 - Requires Decal / Virindi View System (comes with virindi bundle)
 - Install [QuestTrackerInstaller-1.0.0.5.exe](https://gitlab.com/Schneebly/questtracker/uploads/78d72305cf4bc495346ec7d7271d1be3/QuestTrackerInstaller-1.0.0.5.exe)
    
# How to use:
 - Click Refresh List.

# Images:

![Imgur](https://i.imgur.com/DGqEqfo.png)
![Imgur](https://i.imgur.com/hmofTpq.png)
![Imgur](https://i.imgur.com/EgH9iSS.png)

