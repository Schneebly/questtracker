﻿using System;
using System.IO;
using System.Xml;
using System.Linq;
using System.Collections.Generic;
using System.Net;

namespace QuestTracker
{
    public static class Util
    {
        public static void LogError(Exception ex)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.Personal) + @"\Asheron's Call\" + Globals.PluginName + " errors.txt", true))
                {
                    writer.WriteLine("============================================================================");
                    writer.WriteLine(DateTime.Now.ToString());
                    writer.WriteLine("Error: " + ex.Message);
                    writer.WriteLine("Source: " + ex.Source);
                    writer.WriteLine("Stack: " + ex.StackTrace);
                    if (ex.InnerException != null)
                    {
                        writer.WriteLine("Inner: " + ex.InnerException.Message);
                        writer.WriteLine("Inner Stack: " + ex.InnerException.StackTrace);
                    }
                    writer.WriteLine("============================================================================");
                    writer.WriteLine("");
                    writer.Close();
                }
            }
            catch
            {
            }
        }

        public static void WriteToChat(string message)
        {
            try
            {
                Globals.Host.Actions.AddChatText("<{" + Globals.PluginName + "}>: " + message, 5);
            }
            catch (Exception ex) { LogError(ex); }
        }

        public static Dictionary<string, string> questKeyLookup = new Dictionary<string, string>();

        public static void LoadQuestLookupXML()
        {
            try
            {
                string filePath = Util.GetPluginStoragePath() + @"quests.xml";

                if (!File.Exists(filePath))
                {
                    Util.WriteToChat("Unable to find lookup file: " + filePath);
                    return;
                }

                using (XmlReader reader = XmlReader.Create(filePath))
                {
                    while (reader.Read())
                    {
                        if (reader.IsStartElement() && reader.Name != "root")
                        {
                            questKeyLookup.Add(reader.Name.ToLower(), reader.ReadElementContentAsString());
                        }
                    }
                }
            }
            catch (Exception e) { Util.LogError(e); }
        }

        public static string GetFriendlyQuestName(string questKey)
        {
            if (questKeyLookup.Keys.Contains(questKey))
            {
                return questKeyLookup[questKey];
            }

            return questKey;
        }

        public static string GetPluginStoragePath()
        {
            try
            {
                string PluginPath = @"C:\Games\DecalPlugins\" + Globals.PluginName + @"\";
                return PluginPath;
            }
            catch
            {
                throw new NotImplementedException();
            }
            
        }

        internal static void WriteToChat(object completedQueststr)
        {
            throw new NotImplementedException();
        }
    }
}
